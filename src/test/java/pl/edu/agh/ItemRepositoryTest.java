package pl.edu.agh;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import pl.edu.agh.model.Item;

import java.io.File;
import java.io.IOException;

import static org.junit.Assert.*;

public class ItemRepositoryTest {

    @Rule
    public TemporaryFolder temporaryFolder = new TemporaryFolder();

    @Test
    public void testSaveItem() throws IOException  {
        File temp = temporaryFolder.newFile("db.txt");
        ItemRepository itemRepository = new ItemRepository(temp.getAbsolutePath());
        itemRepository.saveItem(new Item(54, "Chair", "Kitchen"));
        assertTrue(itemRepository.findItemById(54).isPresent());
        assertEquals(itemRepository.findItemById(54).get().getId(), 54);
        assertEquals(itemRepository.findItemById(54).get().getName(), "Chair");
        assertEquals(itemRepository.findItemById(54).get().getLocation(), "Kitchen");
    }

    @Test
    public void testFindAllItems() throws IOException {
        File temp = temporaryFolder.newFile("db.txt");
        ItemRepository itemRepository = new ItemRepository(temp.getAbsolutePath());
        itemRepository.saveItem(new Item(54, "Chair", "Kitchen"));
        itemRepository.saveItem(new Item(55, "Table", "Kitchen"));
        assertEquals(2, itemRepository.findAllItems().size());
    }

    @Test
    public void testFindItemsByLocatization() throws IOException {
        File temp = temporaryFolder.newFile("db.txt");
        ItemRepository itemRepository = new ItemRepository(temp.getAbsolutePath());
        itemRepository.saveItem(new Item(54, "Chair", "Kitchen"));
        itemRepository.saveItem(new Item(55, "Table", "Kitchen"));
        assertEquals(2, itemRepository.findItemsByLocatization("Kitchen").size());
    }

    @Test
    public void testFindItemById() throws IOException {
        File temp = temporaryFolder.newFile("db.txt");
        ItemRepository itemRepository = new ItemRepository(temp.getAbsolutePath());
        itemRepository.saveItem(new Item(54, "Chair", "Kitchen"));
        itemRepository.saveItem(new Item(55, "Table", "Kitchen"));
        assertTrue(itemRepository.findItemById(54).isPresent());
        assertEquals(itemRepository.findItemById(54).get().getName(), "Chair");
    }
}