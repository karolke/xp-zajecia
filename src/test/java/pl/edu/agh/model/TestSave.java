package pl.edu.agh.model;

import fit.RowFixture;

import java.util.List;

public class TestSave extends RowFixture {
    @Override
    public Object[] query() throws Exception {
        Item[] array = new Item[2];
        SetUp.saveFile();
        List<Item> items = SetUp.itemService.listAllItems();
        array = items.toArray(array);
        return array;
    }

    @Override
    public Class<?> getTargetClass() {
        return Item.class;
    }
}
