package pl.edu.agh.model;

import fit.Fixture;
import pl.edu.agh.ItemRepository;
import pl.edu.agh.ItemService;

import java.io.File;
import java.io.IOException;

public class SetUp extends Fixture {

    static ItemRepository itemRepository;

    static ItemService itemService;

    public SetUp() {
        System.out.println("Initialize");
        System.out.print("read db....");
    }

    public static void readTestFile() {
        itemRepository = new ItemRepository("test.txt");
        itemService = new ItemService(System.out, itemRepository);
    }

    public static void readTestLocationFile() {
        itemRepository = new ItemRepository("testLocation.txt");
        itemService = new ItemService(System.out, itemRepository);
    }

    public static void saveFile() {
        try {
            File file = new File("testSave.txt");
            file.delete();
            file.createNewFile();
            System.out.println(file.getAbsolutePath());
            itemRepository = new ItemRepository(file.getAbsolutePath());
            itemService = new ItemService(System.out, itemRepository);
            itemRepository.saveItem(new Item(78, "glasses", "bathroom"));
            itemRepository.saveItem(new Item(49, "headphone", "kitchen"));
            itemRepository.saveItem(new Item(14, "cup", "kitchen"));
            itemRepository.saveItem(new Item(53, "charger", "room"));

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void saveInFileDuplicate() {
        try {
            File file = new File("testDuplication.txt");
            file.delete();
            file.createNewFile();
            System.out.println(file.getAbsolutePath());
            itemRepository = new ItemRepository(file.getAbsolutePath());
            itemService = new ItemService(System.out, itemRepository);
            itemService.addItem("Chair","Kitchen");
            itemService.addItem("Chair","Kitchen");
            itemService.addItem("Chair","Kitchen");
            itemService.addItem("Table","Kitchen");
            itemService.addItem("Table","Kitchen");
            itemService.addItem("TV","Room");
            itemService.addItem("TV","Kitchen");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
