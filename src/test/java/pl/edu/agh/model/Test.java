package pl.edu.agh.model;

import fit.ColumnFixture;

public class Test extends ColumnFixture {

    public int id;
    public String name;
    public String location;


    public void execute() {
        SetUp.readTestFile();
        Item item = SetUp.itemService.listAllItems().get(0);
        id = item.getId();
        name = item.getName();
        location = item.getLocation();

    }

    public int id() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String name() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String location() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
}