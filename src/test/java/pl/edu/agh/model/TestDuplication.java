package pl.edu.agh.model;

import fit.RowFixture;

import java.util.List;

public class TestDuplication extends RowFixture {
    @Override
    public Object[] query() throws Exception {
        Item[] array = new Item[4];
        SetUp.saveInFileDuplicate();
        List<Item> items = SetUp.itemService.listAllItems();
        array = items.toArray(array);
        return array;
    }

    @Override
    public Class<?> getTargetClass() {
        return Item.class;
    }
}
