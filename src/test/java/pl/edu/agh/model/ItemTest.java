package pl.edu.agh.model;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ItemTest {

    @Test
    public void willReturnItemSerializedAsString() {
        Item item = new Item(54, "Chair", "Kitchen");
        assertEquals("54,Chair,Kitchen", item.toString());
    }

    @Test
    public void willReturnItemFromString() {
        Item item = Item.fromString("54,Chair,Kitchen");
        assertEquals(54, item.getId());
        assertEquals("Chair", item.getName());
        assertEquals("Kitchen", item.getLocation());
    }
}