package pl.edu.agh;

import pl.edu.agh.model.Item;
import pl.edu.agh.presentation.CLIPresenter;

import java.io.PrintStream;
import java.util.List;

public class ItemService {

    private CLIPresenter cliPresenter;

    private ItemRepository itemRepository;

    public ItemService(PrintStream printStream, ItemRepository itemRepository) {
        cliPresenter = new CLIPresenter(printStream);
        this.itemRepository = itemRepository;
    }

    public List<Item> listAllItems() {
        List<Item> items = itemRepository.findAllItems();
        cliPresenter.printList(items);
        if(items.isEmpty()){
            cliPresenter.printError("There is no items ");
        }
        return items;
    }

    public List<Item> listAllItemsByLocalization(String localization) {
        List<Item> itemsByLocatization = itemRepository.findItemsByLocatization(localization);
        if(itemsByLocatization.isEmpty()) {
            cliPresenter.printError("There is no items in the location");
        }
        cliPresenter.printList(itemsByLocatization);
        return itemsByLocatization;
    }

    public void listAllItemsByOwner(String owner) {
        List<Item> itemsByOwner = itemRepository.findItemsByOwner(owner);
        if(itemsByOwner.isEmpty()) {
            cliPresenter.printError("This owner has no items");
        }
        cliPresenter.printList(itemsByOwner);
    }

    public void addItem(String name, String location) {
       List<Item> itemList = itemRepository.findItemsByName(name);
        int id = itemRepository.findAllItems().size() + 1;
        if( itemList.stream().anyMatch(i->i.getLocation().equals(location))) {
            cliPresenter.printError("Item already exists !");
            return;
        }
        itemRepository.saveItem(new Item(id, name, location));
        cliPresenter.printMessage("Item: " + name + " saved successfully.");
    }
}
