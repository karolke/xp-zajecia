package pl.edu.agh.presentation;

import pl.edu.agh.ItemRepository;
import pl.edu.agh.ItemService;
import pl.edu.agh.model.Item;

import java.util.List;
import java.util.Scanner;

public class CLI {
    public static void main(String[] args) {
        String choice;
        String name;
        String location;
        Scanner scanner = new Scanner(System.in);
        ItemRepository itemRepository = new ItemRepository("./db.txt");
        System.out.println("Witaj w programie do zarzadzania przedmiotami");
        ItemService itemService = new ItemService(System.out, itemRepository);
        while (true) {
            System.out.println("1. Wyswietl przedmioty");
            System.out.println("2. Wyswietl przedmioty po lokalizacji");
            System.out.println("3. Dodaj przedmiot");
            System.out.println("4. Usun przedmiot po lokalizacji");
            System.out.println("5. Usun przedmiot po nazwie");
            System.out.println("6. Usun przedmiot po id");
            System.out.println("7. Zamknij program");
            choice = scanner.nextLine();
            switch (choice) {
            case "1":
                itemService.listAllItems();
                break;
            case "2":
                System.out.println("Podaj lokalizacje");
                location = scanner.nextLine();
                itemService.listAllItemsByLocalization(location);
                break;
            case "3":
                System.out.println("Podaj nazwę przedmiotu");
                name = scanner.nextLine();
                System.out.println("Podaj lokalizacje przedmiotu");
                location = scanner.nextLine();
                itemService.addItem(name, location);
                break;
            case "4":
                System.out.println("Podaj lokalizajce");
                location = scanner.nextLine();
                itemRepository.removeItemByLocation(location);
                break;
            case "5":
                System.out.println("Podaj nazwe");
                name = scanner.nextLine();
                itemRepository.removeItemByName(name);
                break;
            case "6":
                System.out.println("Podaj id");
                String id = scanner.nextLine();
                itemRepository.removeItemById(Integer.parseInt(id));
                break;
            case "7":
                return;
            }
        }
    }
    private static void printList(List<Item> items) {
        items.forEach(x -> System.out.println(x.toString()));
    }
}
