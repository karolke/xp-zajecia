package pl.edu.agh.presentation;

import pl.edu.agh.model.Item;

import java.io.PrintStream;
import java.util.List;

public class CLIPresenter {

    private PrintStream printStream;

    public CLIPresenter(PrintStream printStream) {
        this.printStream = printStream;
    }

    public void printItem(Item item) {
        printStream.println(item.toString());
    }

    public void printList(List<Item> items) {
        printStream.print("--------ItemList----------\n");
        items.forEach(x -> printStream.println(x.toString()));
        printStream.println("--------------------------");
    }

    public void printError(String message) {
        printStream.println("ERROR: " + message + "\n");
    }

    public void printMessage(String message) {
        printStream.println(message + "\n");
    }
}
