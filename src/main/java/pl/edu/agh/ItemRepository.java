package pl.edu.agh;

import pl.edu.agh.model.Item;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class ItemRepository {
    private final String dbPath;

    public ItemRepository(String dbPath) {
        this.dbPath = dbPath;
    }

    public void saveItem(Item item) {
        try {
            Files.write(Paths.get(dbPath), Collections.singletonList(item.toString()), StandardOpenOption.APPEND);
        } catch (IOException e) {
            System.out.println("Exception during save to file");
        }
    }

    public void removeItemById(int id) {
        try {
            List<Item> allItems = findAllItems();

            PrintWriter writer = new PrintWriter(dbPath);
            writer.print("");
            writer.close();

            Optional<Item> itemToRemoval = allItems.stream().filter(e->e.getId()==id).findFirst();

            boolean b = allItems.removeIf(it -> it.getId() == id);

            List<Item> result = new ArrayList<>();
            for (int i = 1; i < allItems.size() + 1; i++) {
                result.add(new Item(i, allItems.get(i - 1).getName(), allItems.get(i - 1).getLocation()));
            }
            Files.write(Paths.get(dbPath), result.stream().map(Item::toString).collect(Collectors.toList()), StandardOpenOption.APPEND);
            if(itemToRemoval.isPresent()){
                System.out.println("Item was removed: "+itemToRemoval.get());
            }
        } catch (IOException e) {
            System.out.println("Exception during delete from file");
        }
    }

    public void removeItemByName(String name) {
        try {
            List<Item> allItems = findAllItems();
            List<Item> newItems = new ArrayList<>(allItems);


            PrintWriter writer = new PrintWriter(dbPath);
            writer.print("");
            writer.close();

            boolean b = newItems.removeIf(it -> it.getName().equals(name));

            List<Item> result = new ArrayList<>();
            for (int i = 1; i < newItems.size() + 1; i++) {
                result.add(new Item(i, newItems.get(i - 1).getName(), newItems.get(i - 1).getLocation()));
            }
            Files.write(Paths.get(dbPath), result.stream().map(Item::toString).collect(Collectors.toList()), StandardOpenOption.APPEND);
            allItems.removeAll(newItems);
            allItems.forEach(e->System.out.println("Item "+e+" was removed"));
        } catch (IOException e) {
            System.out.println("Exception during delete from file");
        }
    }

    public void removeItemByLocation(String location) {
        try {
            List<Item> allItems = findAllItems();
            List<Item> newItems = new ArrayList<>(allItems);

            PrintWriter writer = new PrintWriter(dbPath);
            writer.print("");
            writer.close();

            boolean b = newItems.removeIf(it -> it.getLocation().equals(location));

            List<Item> result = new ArrayList<>();
            for (int i = 1; i < newItems.size() + 1; i++) {
                result.add(new Item(i, newItems.get(i - 1).getName(), newItems.get(i - 1).getLocation()));
            }
            Files.write(Paths.get(dbPath), result.stream().map(Item::toString).collect(Collectors.toList()), StandardOpenOption.APPEND);
            allItems.removeAll(newItems);
            allItems.forEach(e->System.out.println("Item "+e+" was removed"));
        } catch (IOException e) {
            System.out.println("Exception during delete from file");
        }
    }

    public List<Item> findAllItems() {
        try {
            List<Item> result = new ArrayList<>();
            Files.lines(Paths.get(dbPath)).filter(line -> !line.trim().isEmpty()).forEach(it -> result.add(Item.fromString(it)));
            return result;
        } catch (IOException e) {
            System.out.println("Exception during read from file");
        }
        return Collections.emptyList();
    }

    public List<Item> findItemsByLocatization(String localization) {
        return findAllItems().stream().filter(i -> i.getLocation().equals(localization)).collect(Collectors.toList());
    }

    public Optional<Item> findItemById(int id) {
        return findAllItems().stream().filter(i -> i.getId() == id).findFirst();
    }

    public Optional<Item> findItemByName(String name) {
        return findAllItems().stream().filter(i -> i.getName().equals(name)).findFirst();
    }

    public List<Item> findItemsByName(String name) {
        return findAllItems().stream().filter(i -> i.getName().equals(name)).collect(Collectors.toList());
    }


    public List<Item> findItemsByOwner(String owner) {
        return findAllItems().stream().filter(i -> i.getOwner().equals(owner)).collect(Collectors.toList());
    }

//    public boolean removeItemById(int id) {
//        List<Item> items = findAllItems();
//        return items.removeIf(i -> i.getId() == id);
//    }

    public Item updateItem(Item item) {
        findItemById(item.getId()).ifPresent(i -> removeItemById(i.getId()));
        saveItem(item);
        return item;
    }
}
