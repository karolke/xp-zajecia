package pl.edu.agh.model;

import java.util.Objects;
import java.util.Random;

/**
 * Created by lukasz on 05.04.18.
 */
public class Item {

    private int id;
    private String name;
    private String location;
    private String owner;

    public Item(int id, String name, String location) {
        this.id = id;
        this.name = name;
        this.location = location;
    }

    public Item(String name, String location) {
        this.id = new Random().nextInt();
        this.name = name;
        this.location = location;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    @Override
    public String toString() {
        return String.format("%d,%s,%s", id, name, location);
    }

    public static Item fromString(String s) {
        String[] words = s.split(",");
        return new Item(Integer.parseInt(words[0]), words[1], words[2]);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Item item = (Item) o;
        return id == item.id &&
                Objects.equals(name, item.name) &&
                Objects.equals(location, item.location);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, name, location);
    }
}
